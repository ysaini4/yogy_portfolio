$(window).load(function(){
	$('#preloader').fadeOut('slow',function(){$(this).remove();});
});


/******************************************************************************************************************************
Learn More Page Scroll
*******************************************************************************************************************************/
$(function() {
    $('a.page-scroll').bind('click', function(event) {
        var $anchor = $(this);
        $('html, body').stop().animate({
            scrollTop: $($anchor.attr('href')).offset().top
        }, 1500, 'easeInOutExpo');
        event.preventDefault();
    });
});

/******************************************************************************************************************************
Menu
*******************************************************************************************************************************/ 
(function() {

	var bodyEl = document.body,
		//content = document.querySelector( '.content-wrap' ),
		openbtn = document.getElementById( 'open-button' ),
		closebtn = document.getElementById( 'close-button' ),
		toggle_b = document.getElementsByClassName( 'toggle_b' ),
		isOpen = false;

	function init() {
		var winwidth = $( window ).width();
		if (winwidth >= 767){
			openbtn.value =1;
		} else {
			openbtn.value =0;
			classie.remove( bodyEl, 'show-menu' );
		}	
		initEvents();
	}

	function initEvents() {
		for (var i = toggle_b.length - 1; i >= 0; i--) {
			toggle_b[i].addEventListener( 'click', toggleMenu );
		}
		openbtn.addEventListener( 'click', toggleMenu );
		if( closebtn ) {
			closebtn.addEventListener( 'click', toggleMenu );
		}

		/* close the menu element if the target it´s not the menu element or one of its descendants..
		content.addEventListener( 'click', function(ev) {
			var target = ev.target;
			if( isOpen && target !== openbtn ) {
				toggleMenu();
			}
		} );
		*/
	}

	function toggleMenu() {
		var winwidth = $( window ).width();
		console.log(winwidth)
		if (winwidth <= 767){
			if( openbtn.value == 1 ) {
				classie.remove( bodyEl, 'show-menu' );
				openbtn.value = 0;
			}
			else {
				openbtn.value = 1;
				classie.add( bodyEl, 'show-menu' );
			}
			isOpen = !isOpen;
		}
	}

	init();

})();


